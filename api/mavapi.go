// mav api
package api

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"os/exec"
	"sync"
	"time"
	"vighnesh.org/mav/configuration"
	"vighnesh.org/mav/crypto"
	"vighnesh.org/mav/util"
)

var scans = make(map[string]interface{})

type Scanner interface {
	Scan(path string) (string, error)
}

type parser interface {
	parse(output string) string
}

type Antimalware struct {
	Name             string
	Scanner          string
	Options          []string
	Is_file_response bool
	Is_file_scan     bool
}

func Initialize(file *os.File) (map[string]interface{}, error) {
	fileInfo, e := os.Stat(file.Name())
	if e != nil {
		panic(e.Error())
	}
	bytes, _ := ioutil.ReadFile(file.Name())
	hashes := crypto.Hash(bytes)

	var wg sync.WaitGroup
	config := configure()
	for _, av := range config {
		wg.Add(1)
		go execute(file, av, &wg)
	}
	wg.Wait()
	result := make(map[string]interface{})
	date := time.Now()
	result["object"] = fileInfo.Name()
	result["scan_id"] = hashes["SHA256"] + "-" + fmt.Sprint(crypto.HashCode(hashes["MD5"]))
	result["resource"] = hashes["SHA256"]
	result["hashes"] = hashes
	result["scans"] = scans
	result["positives"] = positives(&scans)
	result["total"] = len(config)
	result["scan_date"] = date

	return result, e
}

func positives(scans *map[string]interface{}) int {
	positives := 0
	for _, scan := range *scans {
		ss := scan.(map[string]interface{})
		if ss["detected"] == true {
			positives++
		}
	}
	return positives
}

func execute(file *os.File, av Antimalware, wg *sync.WaitGroup) {
	output, _ := av.scan(file.Name())
	scans[av.Name] = util.Parse(av.Name, output, av.Is_file_response)
	defer wg.Done()
}

func (av Antimalware) scan(path string) (string, error) {
	out, e := exec.Command(av.Scanner, append(av.Options, path)...).Output()
	return string(out), e
}

func configure() map[string]Antimalware {
	antimalwares := make(map[string]Antimalware)
	json.Unmarshal([]byte(configuration.Init()), &antimalwares)
	return antimalwares
}
