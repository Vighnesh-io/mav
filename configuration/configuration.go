package configuration

func Init() string {

	json_string := `{
	"clam":{
	  "Name":"clamav",
      "scanner":"C:/Users/BVR/Downloads/ClamWinPortable/App/clamwin/bin/clamscan.exe",
      "options":[
		 "--database=C:/Users/BVR/Downloads/ClamWinPortable/Data/db",
         "--bytecode=yes",
         "--bytecode-unsigned=yes",
         "--detect-pua=yes",
         "--detect-structured=yes",
         "--heuristic-scan-precedence=yes",
         "--partition-intersection=yes",
         "--algorithmic-detection=yes",
         "--scan-archive=yes",
         "--detect-broken=yes",
         "--enable-stats"
      ],
	"Is_file_response":false,
	"Is_file_scan":true
   },
   "emsisoft":{
	  "Name": "emsisoft",
      "Scanner":"C:/Users/BVR/Downloads/EmsisoftEmergencyKit/bin64/a2cmd.exe",
      "Options":[
         "/archive",
		 "/files"
      ],
	"Is_file_response":false,
	"Is_file_scan":true
   }
}`
	return json_string
}

func Parseoptions() map[string]map[string]string {
	options := make(map[string]map[string]string)
	clamav := make(map[string]string)

	clamav["version"] = `Engine version: (.+)`
	clamav["malware"] = `: (.+) FOUND`
	//clamav["found"] = `Infected files:(.+)`
	clamav["time"] = `Time:(.+)`
	options["clamav"] = clamav

	emsisoft := make(map[string]string)

	emsisoft["version"] = `- Version (.+)`
	emsisoft["malware"] = `detected:(.+)`
	//emsisoft["found"] = `Found(.+)`
	emsisoft["time"] = `Scan time:(.+)`
	options["emsisoft"] = emsisoft

	superantispyware := make(map[string]string)

	superantispyware["version"] = ``
	superantispyware["malware"] = ``
	superantispyware["found"] = ``
	superantispyware["time"] = ``
	options["superantispyware"] = superantispyware

	return options
}
