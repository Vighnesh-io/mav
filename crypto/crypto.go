// crypto
package crypto

import (
	"crypto/md5"
	"crypto/sha1"
	"crypto/sha256"
	"crypto/sha512"
	"encoding/hex"
	"hash/fnv"
)

func HashCode(s string) uint32 {
	hash := fnv.New32a()
	hash.Write([]byte(s))
	return hash.Sum32()
}

func MD5(bytes []byte) string {
	md5 := md5.New()
	md5.Write(bytes)
	return hex.EncodeToString(md5.Sum(nil))
}

func SHA1(bytes []byte) string {
	sha1 := sha1.New()
	sha1.Write(bytes)
	return hex.EncodeToString(sha1.Sum(nil))
}

func SHA256(bytes []byte) string {
	sha256 := sha256.New()
	sha256.Write(bytes)
	return hex.EncodeToString(sha256.Sum(nil))
}

func SHA512(bytes []byte) string {
	sha512 := sha512.New()
	sha512.Write(bytes)
	return hex.EncodeToString(sha512.Sum(nil))
}

func Hash(bytes []byte) map[string]string {
	hashes := make(map[string]string)
	hashes["MD5"] = MD5(bytes)
	hashes["SHA1"] = SHA1(bytes)
	hashes["SHA256"] = SHA256(bytes)
	hashes["SHA512"] = SHA512(bytes)
	return hashes
}
