package server

import (
	"net/http"
	"vighnesh.org/mav/server/serverutil"
	"vighnesh.org/mav/templates/html"
	"vighnesh.org/mav/util"
)

func Start() {

	http.HandleFunc("/upload", upload)
	http.HandleFunc("/file/scan", scan)
	http.ListenAndServe(":8080", nil)
}

func scan(response http.ResponseWriter, request *http.Request) {
	if serverutil.IsPostRequest(response, request) {
		response.Write(util.ToJson(serverutil.HandleRequest(request)))
	}
}

func upload(response http.ResponseWriter, request *http.Request) {
	if serverutil.IsPostRequest(response, request) {
		t := html.HtmlTemplate()
		result := serverutil.WebResponse(serverutil.HandleRequest(request))
		t.Execute(response, result)
	}
}
