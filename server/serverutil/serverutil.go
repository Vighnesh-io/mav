package serverutil

import (
	"io"
	"net/http"
	"os"
	"vighnesh.org/mav/api"
)

func HandleRequest(request *http.Request) map[string]interface{} {
	file := FileReference(request)
	defer file.Close()
	result, _ := api.Initialize(file)
	return result
}

/*func MetaInfo(request *http.Request) map[string]interface{} {
	file := FileReference(request)
	defer file.Close()
	bytes, _ := ioutil.ReadFile(file.Name())
	hashes := crypto.Hash(bytes)
	metaInfo := make(map[string]interface{})
	metaInfo["scan_id"] = hashes["SHA256"] + "-" + crypto.HashCode(hashes["MD5"])
	metaInfo["resource"] = hashes["SHA256"]
	metaInfo["SHA256"] = hashes["SHA256"]
	metaInfo["verbose_msg"] = "Scan request successfully queued, come back later for the report"
	return metaInfo
}
*/

func FileReference(request *http.Request) *os.File {
	request.ParseMultipartForm(1024 * 1024)
	file, header, _ := request.FormFile("file")
	defer file.Close()
	return WriteFileToDisk(file, header.Filename)
}

func IsPostRequest(response http.ResponseWriter, request *http.Request) bool {
	if request.Method == "POST" {
		return true
	} else {
		http.Error(response, "REQUEST GET NOT ALLOWED", http.StatusMethodNotAllowed)
		return false
	}
}
func WriteFileToDisk(file io.Reader, name string) *os.File {
	out, e := os.Create("F:/samples/" + name)
	if e != nil {
		panic(e.Error())
	}
	io.Copy(out, file)
	return out
}

func WebResponse(result map[string]interface{}) map[string]interface{} {
	scans := result["scans"].(map[string]interface{})
	for _, scan := range scans {
		delete(scan.(map[string]interface{}), "detected")
	}

	maps := make(map[string]interface{})
	maps["scans"] = scans
	metadata := make(map[string]interface{})
	metadata["object"] = result["object"]
	metadata["total"] = result["total"]
	metadata["positives"] = result["positives"]
	metadata["SHA256"] = result["resource"]
	metadata["analysys date"] = result["scan_date"]
	maps["metadata"] = metadata
	return maps
}
