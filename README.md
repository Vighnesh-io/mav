# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Mav is a multi-antivius scanner to scan a file with multiple antivirus engines and provides a detailed scan report.

### How do I get set up? ###

* Configuration

Download following antivirus engines

1) https://dl.emsisoft.com/EmsisoftEmergencyKit.exe

2) https://www.clamav.net/downloads/production/clamav-0.101.3-win-x64-portable.zip

3) Update https://bitbucket.org/Vighnesh-io/mav/src/29e2809fcad5ac3bbf55937f1ab06da2cba2ea9c/configuration/configuration.go#lines-8 and 
https://bitbucket.org/Vighnesh-io/mav/src/29e2809fcad5ac3bbf55937f1ab06da2cba2ea9c/configuration/configuration.go#lines-10
to calmwin location in your machine.

4) Update https://bitbucket.org/Vighnesh-io/mav/src/29e2809fcad5ac3bbf55937f1ab06da2cba2ea9c/configuration/configuration.go#lines-27 to emsisoft location in your machine.

5) Run the compiled executable

6) in browser hit http://localhost:8080

upload file, it will scan and give the report