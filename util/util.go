// util
package util

import (
	"encoding/json"
	"regexp"
	"strings"
	"vighnesh.org/mav/configuration"
)

func parse_file_response(response string) string {
	return ""
}

func Parse(antimalware string, output string, is_file_response bool) map[string]interface{} {
	if is_file_response {
		output = parse_file_response(output)
	}
	options := configuration.Parseoptions()

	maps := make(map[string]interface{})
	maps["detected"] = false

	for key, option := range options[antimalware] {
		reg := regexp.MustCompile(option)
		value := reg.FindStringSubmatch(output)
		if value != nil {
			maps[key] = strings.Trim(strings.Trim(value[1], " "), "\r")
			if key == "malware" && reg.MatchString(value[0]) {
				maps["detected"] = true
			}
		} else {
			maps[key] = nil
		}
	}
	return maps
}

func ToJson(maps map[string]interface{}) []byte {
	//json_response, _ := json.Marshal(maps)
	json_formated, _ := json.MarshalIndent(maps, "", "  ")
	return json_formated
}
