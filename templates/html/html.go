package html

import "html/template"

func HtmlTemplate() *template.Template {
	tmpl := template.New("Report")
	t, e := tmpl.Parse(`<!DOCTYPE html>
<html>
<head>
<style>
table {
    font-family: arial, sans-serif;
    border-collapse: collapse;
    width: 50%;
	margin:auto;
}

td, th {
    border-bottom: 1px solid #dddddd;
    text-align: left;
    padding: 8px;
}

tr:hover{background-color:#f5f5f5}

</style>
</head>
<body>
<table class="metadata">
<tbody>
<tr>
<th>SHA256</th>
<td>{{index .metadata "SHA256"}}</td>
</tr>
<tr>
<th>File name</th>
<td>{{index .metadata "object"}}</td>
</tr>
<tr>
<th>Analysis date</th>
<td>{{index .metadata "analysys date"}}</td>
</tr>
</tbody>
</table>
</br></br></br>
<table class="analysis">
<thead>
  <tr>
    <th>Antimalware</th>
    <th>Malware</th>
	<th>Version</th>
	<th>Time</th>
  </tr>
  </thead>
  <tbody>
{{ range $key, $value := .scans }}
  <tr>
    <td>{{$key}}</td>
    <td>{{index $value "malware"}}</td>
	<td>{{index $value "version"}}</td>
	<td>{{index $value "time"}}</td>
  </tr>
{{ end }}
  </tbody>
  <tfoot>
  <tr>
  <td>Total: {{index .metadata "total"}}</td>
  <td>Positives: {{index .metadata "positives"}}</td>
  <td>Detection ratio : {{index .metadata "positives"}}/{{index .metadata "total"}}</td>
  <td></td>
  </tr>
  </tfoot>
</table>

</body>
</html>
`)
	return template.Must(t, e)
}
